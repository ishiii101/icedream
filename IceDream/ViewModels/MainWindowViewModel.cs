﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IceDream.Entities;
using IceDream.Repositories;

namespace IceDream.ViewModels
{
    public class MainWindowViewModel
    {
        // The following 3 properties are linked to the corresponding data grids in the UI, which are updated any time the collection is modified.
        public ObservableCollection<Customer> CustomerList { get; set; } = new ObservableCollection<Customer>();
        public ObservableCollection<Order> OrderList { get; set; } = new ObservableCollection<Order>();
        public ObservableCollection<Item> ItemList { get; set; } = new ObservableCollection<Item>();
        public ObservableCollection<Order_Detail> OrderDetailList { get; set; } = new ObservableCollection<Order_Detail>();
        public ObservableCollection<VIP> VIPList { get; set; } = new ObservableCollection<VIP>();

        public Customer SelectedCustomer { get; set; }
        public Order SelectedOrder { get; set; }
        public Item SelectedItem { get; set; }


        private CustomerRepository customerRepository;
        private OrderRepository orderRepository;
        private ItemRepository itemRepository;
        private OrderDetailRepository orderDetailRepository;
        private VIPRepository vIPRepository;


        Random RNG = new Random(0);

        public MainWindowViewModel()
        {
            LoadData();


           
        }

        /// <summary>
        /// 
        /// </summary>
        public void LoadData()
        {
            // Data load disabled for demo -- No SQL DB available. Using bogus data instead.
            //CustomerList = customerRepository.getAllCustomers();
            //OrderList = orderRepository.getAllOrders();
            //ItemList = itemRepository.getAllItems();
            //VIPList = vIPRepository.getAllVIPs();
            //OrderDetailList = orderDetailRepository.getAllOrderDetails();

            // Generate random values
            for (var i = 0; i < 10; i++)
            {
                CustomerList.Add(new Customer()
                {
                    Address = $"{RNG.Next(0, 10000)} Park Lane",
                    DateOfBirth = DateTime.Today - new TimeSpan(RNG.Next(0, 100000), RNG.Next(0, 24), RNG.Next(0, 60), RNG.Next(0, 60)),
                    FirstName = $"Rando{i}",
                    LastName = "McRando",
                    VipID = RNG.Next(),
                    VipStatus = "Gold"
                });
            }

            for (var i = 0; i < 20; i++)
            {
                ItemList.Add(new Item()
                {
                    Description = "Sweet & creamy!",
                    ItemId = (short)RNG.Next(),
                    Price = (decimal)(RNG.Next(10, 250) / 100f)
                });
            }

            for (var i = 0; i < 50; i++)
            {
                OrderList.Add(new Order()
                {
                    OrderId = RNG.Next(),
                    PurchaseDate = DateTime.Today - new TimeSpan(RNG.Next(0, 100), RNG.Next(0, 24), RNG.Next(0, 60), RNG.Next(0, 60)),
                    Status = (short)RNG.Next(),
                    RequireDate = DateTime.Today + new TimeSpan(RNG.Next(0, 25), RNG.Next(0, 24), RNG.Next(0, 60), RNG.Next(0, 60)),
                    //Order_Detail = new List<Order_Detail>()
                    //{
                    //    new Order_Detail()
                    //    {
                    //        DetailId = RNG.Next(),
                    //        Item = ItemList[RNG.Next(0, ItemList.Count)],
                    //        Quantity = (short)RNG.Next(0, 100)
                    //    }
                    //}

                });
            }

        }

    }
}
