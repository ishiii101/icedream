﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using IceDream.Entities;

namespace IceDream.Windows
{
    /// <summary>
    /// Interaction logic for AddCustomerWindow.xaml
    /// </summary>
    public partial class AddCustomerWindow : Window
    {
        public AddCustomerWindow()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var customer = new Customer
                {
                    FirstName = FirstNameTextBox.Text,
                    LastName = LastNameTextBox.Text,
                    VipID = Convert.ToInt64(VIPIDTextBox.Text),
                    VipStatus = VIPStatusTextBox.Text,
                    Address = AddressTextBox.Text,
                    DateOfBirth = DateTime.Parse(DOBTextBox.Text)
                };

                MainWindow.Instance.ViewModel.CustomerList.Add(customer);
                MainWindow.Instance.CustomerGrid.Items.Refresh();
            }
            catch
            {

            }

            Close();
        }
    }
}
