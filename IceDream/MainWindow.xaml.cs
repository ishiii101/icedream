﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IceDream.Entities;
using IceDream.ViewModels;
using IceDream.Windows;

namespace IceDream
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        /// <summary>
        /// Holds all data for the MainWindow.
        /// </summary>
        public MainWindowViewModel ViewModel;

        public Customer SelectedCustomer => ViewModel.SelectedCustomer;
        public Order SelectedOrder => ViewModel.SelectedOrder;
        public Item SelectedItem => ViewModel.SelectedItem;

        public static MainWindow Instance;

        public MainWindow()
        {
            InitializeComponent();
            Instance = this;
            DataContext = ViewModel;
            ViewModel = new MainWindowViewModel();
            CustomerGrid.ItemsSource = ViewModel.CustomerList;
            OrderGrid.ItemsSource = ViewModel.OrderList;
            ItemGrid.ItemsSource = ViewModel.ItemList;

        }

        private void CustomerGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectedCustomer != null)
            {
                SaveCurrentCustomer();
                CustomerGrid.Items.Refresh();
            }

            ViewModel.SelectedCustomer =
                ViewModel.CustomerList[CustomerGrid.SelectedIndex] ?? ViewModel.CustomerList[0];
            LoadCurrentCustomer();
        }

        private void LoadCurrentCustomer()
        {
            FirstNameTextBox.Text = SelectedCustomer.FirstName;
            LastNameTextBox.Text = SelectedCustomer.LastName;
            AddressTextBox.Text = SelectedCustomer.Address;
            VIPStatusTextBox.Text = SelectedCustomer.VipStatus;
            DOBTextBox.Text = SelectedCustomer.DateOfBirth?.ToShortDateString() ?? "No Data";

        }

        private void SaveCurrentCustomer()
        {
            SelectedCustomer.FirstName = FirstNameTextBox.Text;
            SelectedCustomer.LastName = LastNameTextBox.Text;
            SelectedCustomer.Address = AddressTextBox.Text;
            SelectedCustomer.VipStatus = VIPStatusTextBox.Text;
            SelectedCustomer.DateOfBirth = DateTime.Parse(DOBTextBox.Text);
        }

        private void OrderGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.SelectedOrder = ViewModel.OrderList[OrderGrid.SelectedIndex] ?? ViewModel.OrderList[0];
            LoadCurrentOrder();
        }

        private void LoadCurrentOrder()
        {
            OrderIDTextBox.Text = SelectedOrder.OrderId.ToString();
            PurchaseDateTextBox.Text = SelectedOrder.PurchaseDate?.ToShortDateString()?? "No Data";
            RequiredDateTextBox.Text = SelectedOrder.RequireDate?.ToShortDateString()?? "No Data";
            StatusTextBox.Text = SelectedOrder.Status?.ToString() ?? "No Data";

        }

        private void SaveCurrentOrder()
        {
            //SelectedOrder.OrderId = Convert.ToInt16(OrderIDTextBox.Text);
            //SelectedOrder.PurchaseDate = DateTime.Parse(PurchaseDateTextBox.Text);
            //SelectedOrder.RequireDate = DateTime.Parse(RequiredDateTextBox.Text);
            //SelectedOrder.Status = Convert.ToInt16(StatusTextBox.Text);

        }

        private void ItemGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (SelectedItem != null)
            //{
            //    SaveCurrentItem();
            //    ItemGrid.Items.Refresh();

            //}

            ViewModel.SelectedItem = ViewModel.ItemList[ItemGrid.SelectedIndex] ?? ViewModel.ItemList[0];
            LoadCurrentItem();
        }

        private void LoadCurrentItem()
        {
            ItemIDTextBox.Text = SelectedItem.ItemId.ToString();
            ItemDescriptionTextBox.Text = SelectedItem.Description;
            ItemPriceTextBox.Text = SelectedItem.Price.ToString();
        }

        private void SaveCurrentItem()
        {
            SelectedItem.ItemId = Convert.ToInt16(ItemIDTextBox.Text);
            SelectedItem.Description = ItemDescriptionTextBox.Text;
            SelectedItem.Price = Convert.ToDecimal(ItemPriceTextBox.Text);
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var window = new AddCustomerWindow();
            window.ShowDialog();
        }
    }
}
