﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IceDream.Entities;
using System.Data.Entity;
using System.Collections.ObjectModel;

namespace IceDream.Repositories
{
    public partial class ItemRepository : ICEDreamDBmodel
    {
        ICEDreamDBmodel icedreamcontext;
        public ItemRepository()
        {

        }

        public virtual DbSet<Item> Item { get; set; }

        public ObservableCollection<Item> getAllItems()
        {

            List<Item> items = icedreamcontext.Items.ToList();
            ObservableCollection<Item> itemlist = new ObservableCollection<Item>(Items);

            return itemlist;


        }

        public Item getItemByID(int id)
        {
            Item item = icedreamcontext.Items.Find(id);

            return item;
        }

        public void AddItemByID(Item item)
        {
            icedreamcontext.Items.Add(item);
            icedreamcontext.SaveChanges();
        }

        public void UpdateItemByID(int id, Item itemtoupdate)
        {
            Item Item = icedreamcontext.Items.Find(id);
            if (itemtoupdate.Description != null)
            {
                Item.Description = itemtoupdate.Description;
            }

            if (itemtoupdate.Price != null)
            {
                Item.Price = itemtoupdate.Price;
            }

         

            icedreamcontext.SaveChanges();


        }



    }
}
