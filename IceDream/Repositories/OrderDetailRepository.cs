﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IceDream.Entities;
using System.Data.Entity;
using System.Collections.ObjectModel;

namespace IceDream.Repositories
{
    public partial class OrderDetailRepository : ICEDreamDBmodel
    {
        ICEDreamDBmodel icedreamcontext;
        public OrderDetailRepository()
        {

        }

        public virtual DbSet<Order_Detail> orderdetail { get; set; }

        public ObservableCollection<Order_Detail> getAllOrderDetails()
        {

            List<Order_Detail> orderdetails = icedreamcontext.Order_Details.ToList();
            ObservableCollection<Order_Detail> orderdetailist = new ObservableCollection<Order_Detail>(orderdetails);

            return orderdetailist;


        }

        public Order_Detail getOrderDetailByID(int id)
        {
            Order_Detail orderdetail = icedreamcontext.Order_Details.Find(id);

            return orderdetail;
        }

        public void AddOrderDetail(Order_Detail orderdetail)
        {
            icedreamcontext.Order_Details.Add(orderdetail);
            icedreamcontext.SaveChanges();
        }

        public void UpdateOrderDetailByID(int id, Order_Detail orderdetailtoupdate)
        {
            Order_Detail orderdetail = icedreamcontext.Order_Details.Find(id);
            if (orderdetail.Item != null)
            {
                orderdetail.Item = orderdetailtoupdate.Item;
            }

            if (orderdetail.Order != null)
            {
                orderdetail.Order = orderdetailtoupdate.Order;
            }

            if (orderdetail.Quantity != null)
            {
                orderdetail.Quantity = orderdetailtoupdate.Quantity;
            }

            if(orderdetail.Total!=null)
            {
                orderdetail.Total = orderdetailtoupdate.Total;
            }


            icedreamcontext.SaveChanges();


        }

      



    }
}
