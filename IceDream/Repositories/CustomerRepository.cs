﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IceDream.Entities;
using System.Data.Entity;
using System.Collections.ObjectModel;

namespace IceDream.Repositories
{
   public partial class CustomerRepository: ICEDreamDBmodel
    {
        ICEDreamDBmodel icedreamcontext;
        public CustomerRepository()
        {
            
        }

        public virtual DbSet<Customer> Customer { get; set; }

        public ObservableCollection<Customer> getAllCustomers()
        {

            List<Customer> customers = icedreamcontext.Customers.ToList();
            ObservableCollection<Customer> customerlist = new ObservableCollection<Customer>(customers);

            return customerlist;


        }

        public Customer getCustomerByID(int id)
        {
            Customer customer = icedreamcontext.Customers.Find(id);

            return customer;
        }

        public void AddCustomerByID(Customer customer)
        {
            icedreamcontext.Customers.Add(customer);
            icedreamcontext.SaveChanges();
        }

        public void UpdateCustomerByID(int id,Customer customertoupdate)
        {
            Customer customer = icedreamcontext.Customers.Find(id);
            if(customertoupdate.Address!=null)
            {
                customer.Address = customertoupdate.Address;
            }

            if(customertoupdate.FirstName!=null)
            {
                customer.FirstName = customertoupdate.FirstName;
            }
           
            if(customertoupdate.LastName!=null)
            {
                customer.LastName = customertoupdate.LastName;
            }

            if(customertoupdate.VipStatus!=null)
            {
                customer.VipStatus = customertoupdate.VipStatus;
            }

            icedreamcontext.SaveChanges();
           
            
        }



    }
}
