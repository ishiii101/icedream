﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IceDream.Entities;
using System.Data.Entity;
using System.Collections.ObjectModel;

namespace IceDream.Repositories
{
    public partial class OrderRepository : ICEDreamDBmodel
    {
        ICEDreamDBmodel icedreamcontext;
        public OrderRepository()
        {

        }

        public virtual DbSet<Order> Order { get; set; }

        public ObservableCollection<Order> getAllOrders()
        {

            List<Order> Orders = icedreamcontext.Orders.ToList();
            ObservableCollection<Order> orderlist = new ObservableCollection<Order>(Orders);

            return orderlist;


        }

        public Order getOrderByID(int id)
        {
            Order order = icedreamcontext.Orders.Find(id);

            return order;
        }

        public void AddOrder(Order order)
        {
            icedreamcontext.Orders.Add(order);
            icedreamcontext.SaveChanges();
        }

        public void UpdateOrderByID(int id, Order ordertoupdate)
        {
            Order order = icedreamcontext.Orders.Find(id);
            if (ordertoupdate.PurchaseDate != null)
            {
                order.PurchaseDate = ordertoupdate.PurchaseDate;
            }

            if (ordertoupdate.RequireDate != null)
            {
                order.RequireDate = ordertoupdate.RequireDate;
            }

            if(ordertoupdate.Status!=null)
            {
                order.Status = ordertoupdate.Status;
            }



            icedreamcontext.SaveChanges();


        }



    }
}
