﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IceDream.Entities;
using System.Data.Entity;
using System.Collections.ObjectModel;

namespace IceDream.Repositories
{
    public partial class VIPRepository : ICEDreamDBmodel
    {
        ICEDreamDBmodel icedreamcontext;
        public VIPRepository()
        {

        }

        public virtual DbSet<VIP> VIP { get; set; }

        public ObservableCollection<VIP> getAllVIPs()
        {

            List<VIP> vips = icedreamcontext.VIPs.ToList();
            ObservableCollection<VIP> viplist = new ObservableCollection<VIP>(vips);

            return viplist;


        }

        public VIP getVIPByID(int id)
        {
            VIP vip = icedreamcontext.VIPs.Find(id);

            return vip;
        }

        public void AddVIP(VIP vip)
        {
            icedreamcontext.VIPs.Add(vip);
            icedreamcontext.SaveChanges();
        }

        public void UpdateVIPByID(int id, VIP viptoupdate)
        {
            VIP vip = icedreamcontext.VIPs.Find(id);
            if (viptoupdate.Customer != null)
            {
                vip.Customer = viptoupdate.Customer;
            }

            if (viptoupdate.OrderId != null)
            {
                vip.OrderId = viptoupdate.OrderId;
            }

            if (viptoupdate.PointsEarned != null)
            {
                vip.PointsEarned = viptoupdate.PointsEarned;
            }



            icedreamcontext.SaveChanges();


        }

        public void updateVIPPoints(int id,short points)
        {
            VIP vip = icedreamcontext.VIPs.Find(id);
            vip.PointsEarned = points;

            icedreamcontext.SaveChanges();
        }



    }
}
